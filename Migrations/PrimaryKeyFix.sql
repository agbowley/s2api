﻿SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `votes` ENGINE=INNODB;
ALTER TABLE `teams` ENGINE=INNODB;
ALTER TABLE `servers` ENGINE=INNODB;
ALTER TABLE `playerstats` ENGINE=INNODB;
ALTER TABLE `players` ENGINE=INNODB;
ALTER TABLE `playerinfos` ENGINE=INNODB;
ALTER TABLE `matches` ENGINE=INNODB;
ALTER TABLE `maps` ENGINE=INNODB;
ALTER TABLE `items` ENGINE=INNODB;
ALTER TABLE `images` ENGINE=INNODB;
ALTER TABLE `downloads` ENGINE=INNODB;
ALTER TABLE `commanderstats` ENGINE=INNODB;
ALTER TABLE `commanders` ENGINE=INNODB;
ALTER TABLE `clans` ENGINE=INNODB;
ALTER TABLE `categories` ENGINE=INNODB;
ALTER TABLE `buddies` ENGINE=INNODB;
ALTER TABLE `actionplayers` ENGINE=INNODB;
ALTER TABLE `achievements` ENGINE=INNODB;

ALTER TABLE achievements
  ADD CONSTRAINT achievement_PK
    PRIMARY KEY (id);

ALTER TABLE badges
    ADD CONSTRAINT account_achievement_badge_PK
        PRIMARY KEY (account_id, achievement_id);

ALTER TABLE bans
    ADD COLUMN created_at DATETIME;

ALTER TABLE bans
    ADD COLUMN id INT PRIMARY KEY AUTO_INCREMENT;

TRUNCATE buddies;

ALTER TABLE buddies
    ADD CONSTRAINT source_target_buddies_PK
        PRIMARY KEY(source_id, target_id);

ALTER TABLE buddies
    ADD CONSTRAINT buddies_source_FK
        FOREIGN KEY (source_id) REFERENCES users(id);

ALTER TABLE buddies
    ADD CONSTRAINT buddies_target_FK
        FOREIGN KEY (target_id) REFERENCES users(id);

ALTER TABLE categories
    ADD CONSTRAINT categories_image_FK
        FOREIGN KEY (image_id) REFERENCES images(id);

ALTER TABLE downloads
    ADD CONSTRAINT downloads_image_FK
        FOREIGN KEY (image_id) REFERENCES images(id);

ALTER TABLE downloads
    ADD CONSTRAINT downloads_category_FK
        FOREIGN KEY (categorie_id) REFERENCES categories(id);

ALTER TABLE items
    ADD COLUMN id INT PRIMARY KEY AUTO_INCREMENT;

ALTER TABLE items
    ADD CONSTRAINT items_account_FK
        FOREIGN KEY (account_id) REFERENCES users(id);

ALTER TABLE karmas
    ADD CONSTRAINT karmas_PK
        PRIMARY KEY (account_id, match_id);

ALTER TABLE playerinfos
    ADD CONSTRAINT playerinfos_users_FK
        FOREIGN KEY (account_id) REFERENCES users(id);

ALTER TABLE playerstats
    ADD CONSTRAINT playerstats_users_FK
        FOREIGN KEY (account_id) REFERENCES users(id);

ALTER TABLE votes
    ADD CONSTRAINT votes_account_FK
        FOREIGN KEY (account_id) REFERENCES users(id);

ALTER TABLE votes
    ADD CONSTRAINT votes_comm_FK
        FOREIGN KEY (comm_id) REFERENCES users(id);

TRUNCATE players;

ALTER TABLE `masterserver`.`players`   
	CHANGE `server` `server` INT(11) NULL;

ALTER TABLE `masterserver`.`players` ADD CONSTRAINT `players_servers_FK` FOREIGN KEY (`server`) REFERENCES `masterserver`.`servers`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `masterserver`.`players` ADD CONSTRAINT `players_users_FK` FOREIGN KEY (`account_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `masterserver`.`actionplayers`  
  ADD CONSTRAINT `FK_actionplayers_user` FOREIGN KEY (`account_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_actionplayers_matches` FOREIGN KEY (`match_id`) REFERENCES `masterserver`.`matches`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_actionplayers_` FOREIGN KEY (`team_id`) REFERENCES `masterserver`.`teams`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `masterserver`.`badges`  
  ADD CONSTRAINT `FK_badges_achivements` FOREIGN KEY (`achievement_id`) REFERENCES `masterserver`.`achievements`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `masterserver`.`commanders`  
  ADD CONSTRAINT `FK_commanders_account` FOREIGN KEY (`account_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_commanders_match` FOREIGN KEY (`match_id`) REFERENCES `masterserver`.`matches`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_commanders_teams` FOREIGN KEY (`team_id`) REFERENCES `masterserver`.`teams`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `masterserver`.`playerinfos`  
  ADD CONSTRAINT `FK_playersinfos_account` FOREIGN KEY (`account_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `masterserver`.`playerinfos`   
	CHANGE `account_id` `account_id` INT(11) DEFAULT 0 NOT NULL,
  ADD CONSTRAINT `FK_playersinfos_account` FOREIGN KEY (`account_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `masterserver`.`votes`  
  ADD CONSTRAINT `FK_votes_matches` FOREIGN KEY (`match_id`) REFERENCES `masterserver`.`users`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `masterserver`.`karmas`   
	CHANGE `target_id` `target_id` INT(11) NOT NULL, 
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`account_id`, `target_id`, `match_id`);
