﻿namespace S2Api.Utils
{
    public static class StringUtils
    {
        public static string IfEmptyThenUse(this string original, string newString)
        {
            return string.IsNullOrEmpty(original) ? newString : original;
        }
    }
}
