﻿using PhpSerializerNET;
using S2Api.Models.MasterServer;
using S2Api.Models.Stats;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Threading.Tasks;
using S2Api.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using S2Api.Repository;

namespace S2Api.Services
{
    public interface ISubmitStatsService
    {
        Task<bool> SubmitStats(string login, string pass, int svr_id, int match_id, string map, int winner, string player_stats, string commander_stats, TimeOnly time, string team);
    }
    public class SubmitStatsService : ISubmitStatsService
    {
        private readonly MasterServerContext _context;
        private readonly IUserRepository _userRepo;
        public SubmitStatsService(MasterServerContext context, IUserRepository userRepo)
        {
            _context = context;
            _userRepo = userRepo;
        }

        public async Task<bool> SubmitStats(string login, string pass, int svr_id, int match_id, string map, int winner, string player_stats, string commander_stats, TimeOnly time, string team)
        {
            if (!await _userRepo.IsUserValidated(login, pass)) {
                return false;
            }

            if(!await DoesMatchExistAsync(match_id, svr_id)) {
                return false;
            }

            if (!IsLongerThanFiveMinutes(time))
            {
                return false;
            }

            if(await HasMatchAlreadyBeenSent(match_id))
            {
                return false;
            }

            var executionStrategy = _context.Database.CreateExecutionStrategy();
            return await executionStrategy.ExecuteAsync(async () =>
            {
                var transaction = await _context.Database.BeginTransactionAsync();
                try
                {
                    var commanderStats = PhpSerialization.Deserialize<List<CommanderDTO>>(commander_stats);
                    var playerStats = PhpSerialization.Deserialize<List<ActionPlayerDTO>>(player_stats);
                    var teamStats = PhpSerialization.Deserialize<List<TeamDTO>>(team);

                    if (commanderStats.Any(y => y.RecStats == 0) || playerStats.Any(y => y.RecStats == 0))
                    {
                        return false;
                    }

                    var match = new Match()
                    {
                        Id = match_id,
                        Map = map,
                        Winner = winner,
                        Duration = time
                    };

                    _context.Matches.Add(match);

                    var teamStatsToAdd = teamStats.Select(x => new Team()
                    {
                        Race = x.Race == "H" ? "1" : "2",
                        AvgSf = int.Parse(x.AvgSf),
                        Match = match_id,
                        Commander = x.Commander
                    }).ToList();

                    _context.Teams.AddRange(teamStatsToAdd);

                    await _context.SaveChangesAsync();

                    var winningTeam = teamStats.FirstOrDefault(x => x.Race == winner.ToString());

                    var commanderStatsToAdd = commanderStats.Select(x => new Commander()
                    {
                        Debuffs = x.Debuffs,
                        AccountId = x.AccountId,
                        Buffs = x.Buffs,
                        Builds = x.Builds,
                        EndStatus = x.EndStatus,
                        Exp = x.Exp,
                        Gold = x.Gold,
                        HpHealed = x.HpHealed,
                        Ip = x.Ip,
                        Kills = x.Kills,
                        Orders = x.Orders,
                        Pdmg = x.PDmg,
                        MatchId = match_id,
                        Razed = x.Razed,
                        Secs = x.Secs,
                        Sf = x.Sf,
                        TeamId = teamStatsToAdd.Where(y => y.Race == x.Team.ToString()).Select(y => y.Id).SingleOrDefault()
                    }).ToList();

                    _context.Commanders.AddRange(commanderStatsToAdd);

                    var actionPlayers = playerStats.Where(x => x.Secs > 300).Select(x => new Actionplayer()
                    {
                        AccountId = x.AccountId,
                        Deaths = x.Deaths,
                        Assists = x.Assists,
                        EndStatus = x.EndStatus,
                        Exp = x.Exp,
                        Gold = x.Gold,
                        Bdmg = x.Bdmg,
                        HpHealed = x.HpHealed,
                        Ip = x.Ip,
                        Kills = x.Kills,
                        MatchId = match_id,
                        Pdmg = x.Pdmg,
                        Npc = x.Npc,
                        HpRepaired = x.HpRepaired,
                        Razed = x.Razed,
                        Res = x.Res,
                        Secs = x.Secs,
                        Sf = x.Sf,
                        Souls = x.Souls,
                        TeamId = teamStatsToAdd.Where(y => y.Race == x.Team.ToString()).Select(y => y.Id).SingleOrDefault()
                    }).ToList();

                    _context.Actionplayers.AddRange(actionPlayers);

                    await _context.SaveChangesAsync();

                    await transaction.CommitAsync();

                    return true;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
                finally
                {
                    await transaction.DisposeAsync();
                }
            });
        }

        private async Task<bool> DoesMatchExistAsync(int matchId, int serverId)
        {
            return await _context.MatchSumms.AnyAsync(x => x.Id == matchId && serverId == x.ServerId);
        }

        private bool IsLongerThanFiveMinutes(TimeOnly time)
        {
            return time >= new TimeOnly(0, 5, 0);
        }

        private async Task<bool> HasMatchAlreadyBeenSent(int matchId)
        {
            return await _context.Matches.AnyAsync(x => x.Id == matchId);
        }
    }
}
