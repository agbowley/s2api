using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace S2Api.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }

    public class EmailService : IEmailService
    {
        private readonly SendGridClient _client;
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _client = new SendGridClient(configuration["TwilioApiKey"]);
            _configuration = configuration;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            string htmlTemplate = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title>Welcome to Savage 2</title>
                    <style>
                        .container {
                            width: 100%;
                            font-family: Arial, sans-serif;
                            color: #ffffff;
                            background: linear-gradient(27deg, #151515 5px, transparent 5px) 0 5px,
                                linear-gradient(207deg, #151515 5px, transparent 5px) 10px 0px,
                                linear-gradient(27deg, #111 5px, transparent 5px) 0px 10px,
                                linear-gradient(207deg, #111 5px, transparent 5px) 10px 5px,
                                linear-gradient(90deg, #1b1b1b 10px, transparent 10px),
                                linear-gradient(#1d1d1d 25%, #1a1a1a 25%, #1a1a1a 50%, transparent 50%, transparent 75%, #242424 75%, #242424);
                            background-color: #131313;
                            background-size: 3px 4px;
                        }
                        .content {
                            width: 100%;
                            border: 1px solid #ddd;
                            padding: 50px;
                            text-align: center;
                            box-shadow: 0px 0px 10px 0px #aaa;
                        }
                        a.button {
                            display: inline-block;
                            color: #ffffff;
                            background-color: #c98200;
                            padding: 15px 30px;
                            text-decoration: none;
                            font-size: 18px;
                            margin-top: 20px;
                        }
                    </style>
                </head>
                <body>
                    <div class='container'>
                        <div class='content'>
                            {message}
                        </div>
                    </div>
                </body>
                </html>
                ";

            string emailContent = htmlTemplate
                .Replace("{message}", message);

            var noReplyBaseAddress = _configuration["NoReplyBaseAddress"];
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(noReplyBaseAddress, "Savage 2"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = emailContent
            };
            msg.AddTo(new EmailAddress(email));

            msg.SetClickTracking(false, false);

            await _client.SendEmailAsync(msg);
        }
    }
}