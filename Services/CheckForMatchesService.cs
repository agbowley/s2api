﻿using Discord;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using S2Api.Models.Discord;
using S2Api.Models.GameServers;
using S2Api.Models.Servers;
using S2Api.Repository;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace S2Api.Services
{
    public class CheckForMatchesService : BackgroundService
    {
        private readonly CheckForMatchesDTO _config;
        private readonly IMemoryCache _cache;
        private readonly IServiceProvider _serviceProvider;
        private readonly IDiscordWebhookService _discordClient;
        public CheckForMatchesService(IConfiguration config, IMemoryCache cache, IServiceProvider serviceProvider, IDiscordWebhookService discordClient)
        {
            _config = config.GetSection("CheckForMatches").Get<CheckForMatchesDTO>();
            _cache = cache;
            _serviceProvider = serviceProvider;
            _discordClient = discordClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using var scope = _serviceProvider.CreateScope();
                var serverRepository = scope.ServiceProvider.GetRequiredService<IServerRepository>();

                var servers = await serverRepository.GetOnlineServerList();

                foreach (var server in servers)
                {
                    var cacheItem = _cache.Get<GameServerMatchDTO>(server.Name);
                    var isNextMap = server.Map != cacheItem?.Server.Map;
                    if (server.NumConnected >= _config.MinConnected && isNextMap)
                    {
                        var isPingNeeded = cacheItem?.LastPing == null || cacheItem.LastPing.AddHours(_config.HoursForNextPing) <= DateTime.Now;
                        var discordMessage = CreateDiscordObject(server, isPingNeeded);

                        _cache.Set(server.Name, new GameServerMatchDTO(await _discordClient.SendMessage(discordMessage), server, isPingNeeded ? DateTime.Now : null));
                    }
                    else if (cacheItem?.Id != null)
                    {
                        await _discordClient.EditMessage(cacheItem.Id, CreateDiscordObject(server, false));
                    }
                }

                await Task.Delay(_config.Delay, stoppingToken);
            }
        }

        /// <summary>
        /// Create an object with server data to send to discord's webhook.
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        private DiscordMessageDTO CreateDiscordObject(GameServerModel server, bool isPingNeeded = false)
        {
            var discordMessage = new DiscordMessageDTO()
            {
                Message = $"A new game has started! Come join now.{(isPingNeeded ? $" <@&{_config.RoleId}>" : "")}",
            };

            var embedBuilder = new EmbedBuilder
            {
                Title = $"New game on {server.Name}",
                Description = "There are enough players for a game."
            };

            embedBuilder.AddField("Server Name", server.Name, true);
            embedBuilder.AddField("Number connected", $"{server.NumConnected}/{server.MaxConnected}", true);
            embedBuilder.AddField("Status", server.Status, true);
            embedBuilder.AddField("Server address", server.Ip, true);
            embedBuilder.AddField("Map", server.Map, true);
            embedBuilder.AddField("Next map", server.NextMap, true);
            embedBuilder.WithCurrentTimestamp();

            var embed = embedBuilder.Build();

            discordMessage.Embeds = new System.Collections.Generic.List<Embed>
            {
                embed
            };

            return discordMessage;
        }
    }
}
