﻿using PhpSerializerNET;
using System.Text.Json.Serialization;

namespace S2Api.Models.Stats
{
    public class TeamDTO
    {
        [PhpProperty("avg_sf")]
        public string AvgSf { get; set; }
        [PhpProperty("race")]
        public string Race { get; set; }
        [PhpProperty("clan_name")]
        public string ClanName { get; set; }
        [PhpProperty("commander")]
        public string Commander { get; set; }
    }
}
