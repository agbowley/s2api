﻿namespace S2Api.Models.Servers
{
    public class CheckForMatchesDTO
    {
        public int Delay { get; set; }
        public int MinConnected { get; set; }
        public ulong RoleId { get; set; }
        public int HoursForNextPing { get; set; }
    }
}
