﻿using S2Api.Models.GameServers;
using System;

namespace S2Api.Models.Servers
{
    public class GameServerMatchDTO
    {
        public GameServerMatchDTO()
        {

        }

        public GameServerMatchDTO(ulong id, GameServerModel server, DateTime? lastPing = null)
        {
            Id = id;
            Server = server;
            if (lastPing != null)
            {
                LastPing = lastPing.Value;
            }
            
        }
        public ulong Id { get; set; }
        public GameServerModel Server { get; set; }
        public DateTime LastPing { get; set; }
    }
}
