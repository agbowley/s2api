﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Models.GameServers
{
    public class GameServerModel
    {
        public bool Official { get; set; }
        public string Name { get; set; }
        public string Ip { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Map { get; set; }
        public string NextMap { get; set; }
        public int NumConnected { get; set; }
        public int MaxConnected { get; set; }
        public List<string> Players { get; set; }
    }
}
