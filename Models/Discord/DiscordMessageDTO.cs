﻿using Discord;
using System.Collections.Generic;

namespace S2Api.Models.Discord
{
    public class DiscordMessageDTO
    {
        public List<Embed> Embeds { get; set; }
        public string Message { get; set; }
    }
}
