﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Clan
    {
        public int Id { get; set; }
        public string ClanName { get; set; }
        public string ClanTag { get; set; }
        public string ClanImg { get; set; }
    }
}
