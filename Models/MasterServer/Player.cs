﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Player
    {
        public int AccountId { get; set; }
        public int? Server { get; set; }
        public DateTime? Updated { get; set; }
        public int? Online { get; set; }

        public virtual User Account { get; set; }
        public virtual Server ServerNavigation { get; set; }
    }
}
