﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Buddy
    {
        public int SourceId { get; set; }
        public int TargetId { get; set; }
        public string Note { get; set; }
        public string ClanName { get; set; }
        public string ClanTag { get; set; }
        public string ClanImg { get; set; }
        public string Avatar { get; set; }

        public virtual User Source { get; set; }
        public virtual User Target { get; set; }
    }
}
