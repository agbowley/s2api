﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Map
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
