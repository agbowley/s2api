﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Vote
    {
        public int AccountId { get; set; }
        public int CommId { get; set; }
        public int MatchId { get; set; }
        public int Vote1 { get; set; }
        public string Reason { get; set; }

        public virtual User Account { get; set; }
        public virtual User Comm { get; set; }
        public virtual User Match { get; set; }
    }
}
