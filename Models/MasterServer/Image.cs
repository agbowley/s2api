﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Image
    {
        public Image()
        {
            Categories = new HashSet<Category>();
            Downloads = new HashSet<Download>();
        }

        public int Id { get; set; }
        public string ImageUrl { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Download> Downloads { get; set; }
    }
}
