﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using S2Api.Models.Authentication;

namespace S2Api.Models.MasterServer
{
    public partial class User
    {
        public User()
        {
            Actionplayers = new HashSet<Actionplayer>();
            Articles = new HashSet<Article>();
            Badges = new HashSet<Badge>();
            Bans = new HashSet<Ban>();
            BuddySources = new HashSet<Buddy>();
            BuddyTargets = new HashSet<Buddy>();
            Commanders = new HashSet<Commander>();
            Items = new HashSet<Item>();
            KarmaAccounts = new HashSet<Karma>();
            KarmaTargets = new HashSet<Karma>();
            VoteAccounts = new HashSet<Vote>();
            VoteComms = new HashSet<Vote>();
            VoteMatches = new HashSet<Vote>();
            RefreshTokens = new HashSet<RefreshToken>();
            EmailConfirmationTokens = new HashSet<EmailConfirmationToken>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public sbyte? Permissions { get; set; }
        public string Password { get; set; }
        public string TempPassword { get; set; }
        public string Cookie { get; set; }
        public string RememberToken { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? LastVerificationEmailSentAt { get; set; }
        public string ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordExpiration { get; set; }

        public virtual Player Player { get; set; }
        public virtual Playerinfo Playerinfo { get; set; }
        public virtual ICollection<Actionplayer> Actionplayers { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Badge> Badges { get; set; }
        public virtual ICollection<Ban> Bans { get; set; }
        public virtual ICollection<Buddy> BuddySources { get; set; }
        public virtual ICollection<Buddy> BuddyTargets { get; set; }
        public virtual ICollection<Commander> Commanders { get; set; }
        public virtual ICollection<Item> Items { get; set; }
        public virtual ICollection<Karma> KarmaAccounts { get; set; }
        public virtual ICollection<Karma> KarmaTargets { get; set; }
        public virtual ICollection<Vote> VoteAccounts { get; set; }
        public virtual ICollection<Vote> VoteComms { get; set; }
        public virtual ICollection<Vote> VoteMatches { get; set; }
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
        public virtual ICollection<EmailConfirmationToken> EmailConfirmationTokens { get; set; }
        public virtual UserStash UserStash { get; set; }
    }
}
