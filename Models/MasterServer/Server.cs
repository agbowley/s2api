﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Server
    {
        public Server()
        {
            Players = new HashSet<Player>();
        }

        public int Id { get; set; }
        public string Ip { get; set; }
        public int? Port { get; set; }
        public int? NumConn { get; set; }
        public int? MaxConn { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Description { get; set; }
        public int? Minlevel { get; set; }
        public int? Maxlevel { get; set; }
        public string Official { get; set; }
        public string Status { get; set; }
        public string Map { get; set; }
        public string NextMap { get; set; }
        public DateTime? Updated { get; set; }
        public bool? Online { get; set; }

        public virtual ICollection<Player> Players { get; set; }
    }
}
