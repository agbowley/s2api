﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Category
    {
        public Category()
        {
            Downloads = new HashSet<Download>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ImageId { get; set; }

        public virtual Image Image { get; set; }
        public virtual ICollection<Download> Downloads { get; set; }
    }
}
