﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Karma
    {
        public int AccountId { get; set; }
        public int TargetId { get; set; }
        public int MatchId { get; set; }
        public string Do { get; set; }
        public string Reason { get; set; }

        public virtual User Account { get; set; }
        public virtual MatchSumm Match { get; set; }
        public virtual User Target { get; set; }
    }
}
