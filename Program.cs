using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Minio.AspNetCore;
using S2Api.Models;
using S2Api.Models.S3;
using S2Api.Repository;
using S2Api.Services;
using S2Api.Utils;
using Serilog;
using Serilog.Events;
using System;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning).Enrich.FromLogContext().WriteTo.File("log.txt", rollingInterval: RollingInterval.Day).WriteTo.Console().CreateLogger();

try
{
	var mapperConfig = new MapperConfiguration(cfg =>
	{
		cfg.AddProfile<MappingProfile>();
	});

	var mapper = mapperConfig.CreateMapper();

	var builder = WebApplication.CreateBuilder(args);
	builder.Logging.AddSerilog(Log.Logger);

	builder.Services.AddControllers();

	builder.Services.AddEndpointsApiExplorer();
	builder.Services.AddSwaggerGen(options =>
	{
		options.SwaggerDoc("v1", new OpenApiInfo
		{
			Version = "v1",
			Title = "S2Api",
			Description = "Savage2 Api"
		});
	});


	builder.Services.AddAuthentication(options =>
	{
		options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
		options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
	})
	.AddJwtBearer(options =>
	{
		options.TokenValidationParameters = new TokenValidationParameters
		{
			ValidateIssuerSigningKey = true,
			IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
			ValidateIssuer = false,
			ValidateAudience = false,
			ClockSkew = TimeSpan.Zero,
			ValidateLifetime = true
		};
	});

	builder.Services.AddAuthorization(options =>
	{
		options.AddPolicy("JwtQuery", policy =>
		{
			policy.Requirements.Add(new JwtQueryRequirement());
		});
	});

	builder.Services.AddSingleton<IDiscordWebhookService, DiscordWebhookService>();
	builder.Services.AddSingleton<IHostedService, CheckForMatchesService>();
	builder.Services.AddSingleton<IRefreshTokenService, RefreshTokenService>();
	builder.Services.AddSingleton<IEmailService, EmailService>();
	builder.Services.AddSingleton<IAuthorizationHandler, JwtQueryHandler>();
	builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
	builder.Services.AddSingleton<IEventService, EventService>();

	builder.Services.AddSingleton(mapper);
	builder.Services.AddScoped<IServerRepository, ServerRepository>();
	builder.Services.AddScoped<IJwtFactory, JwtFactory>();
	builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
	builder.Services.AddScoped<IUserRepository, UserRepository>();
	builder.Services.AddScoped<IStatsService, StatsService>();
	builder.Services.AddScoped<ISubmitStatsService, SubmitStatsService>();
	builder.Services.AddScoped(provider =>
	{
		var response = provider.GetRequiredService<HttpResponse>();
		var stream = response.Body;
		return new EventStreamWriter(stream);
	});


	var config = builder.Configuration.GetSection("S3").Get<S3ConfigModel>();

	builder.Services.AddMinio().Configure<MinioOptions>("ovh", options =>
	{
		options.Endpoint = config.Endpoint;
		options.Region = config.Region;
		options.AccessKey = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID").IfEmptyThenUse(builder.Configuration["AwsAccessKeyId"]);
		options.SecretKey = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY").IfEmptyThenUse(builder.Configuration["AwsSecretAccessKey"]);
		options.ConfigureClient(client =>
		{
			client.WithSSL();
		});
	});

	builder.Services.AddCors(options =>
	{
		options.AddPolicy(name: "CorsPolicy",
			builder =>
			{
				builder.WithOrigins("http://localhost:3000", "https://localhost:3000", "s2-website")
						.AllowAnyHeader()
						.AllowAnyMethod()
						.AllowCredentials();

			});
	});

	builder.Services.AddMemoryCache();

	var connectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING").IfEmptyThenUse(builder.Configuration.GetConnectionString("MasterServerContext"));

	builder.Services.AddDbContextPool<MasterServerContext>(options =>
	{
		options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString), builder => builder.EnableRetryOnFailure());
	});

	var app = builder.Build();

	//try
	//{
	//	EnsureDatabaseMigrated(app).GetAwaiter().GetResult();
	//}
	//catch (Exception ex)
	//{
	//	Console.WriteLine(ex);
	//}

	app.UseHttpsRedirection();

	app.UseSwagger();
	app.UseSwaggerUI();

	app.UseRouting();

	app.UseCors("CorsPolicy");

	app.UseAuthentication();
	app.UseAuthorization();

	app.UseEndpoints(endpoints =>
	{
		endpoints.MapControllers().RequireCors("CorsPolicy");
	});

	//static async System.Threading.Tasks.Task EnsureDatabaseMigrated(IApplicationBuilder app)
	//{
	//	try
	//	{
	//		using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
	//		var context = serviceScope.ServiceProvider.GetService<MasterServerContext>();
	//		await context.Database.MigrateAsync();
	//	}
	//	catch (Exception ex)
	//	{
	//		Console.WriteLine(ex);
	//	}
	//}

	app.Run();
}
catch (Exception ex)
{
	Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
	Log.CloseAndFlush();
}