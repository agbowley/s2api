﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S2Api.Models;
using S2Api.Models.MasterServer;
using S2Api.Models.Stats;
using S2Api.Services;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        private readonly IStatsService _statsService;

        public StatsController(IStatsService statsService)
        {
            _statsService = statsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPlayerStatsAsync([FromQuery] int page, [FromQuery] int pageSize, [FromQuery] string sortBy, [FromQuery] bool isAscending, [FromQuery] string filter)
        {
            try
            {
                var matches = await _statsService.GetAllPlayerStatsAsync(page, pageSize, sortBy, isAscending, filter);
                return Ok(matches);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving players.");
            }
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> GetPlayerStatsByUsername(string username)
        {
            try
            {
                var playerStats = await _statsService.GetPlayerStatsByUsernameAsync(username);

                if (playerStats == null)
                {
                    return NotFound();
                }

                return Ok(playerStats);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving the player.");
            }
        }
    }
}