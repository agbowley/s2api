﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.Players;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly MasterServerContext _context;
        public PlayersController(MasterServerContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<PlayerModel>), 200)]
        public async Task<IActionResult> Index()
        {
            //todo: make a foreign key between clan and playerinfo clan_id
            var clans = await _context.Clans.ToListAsync();
            return Ok(await _context.Users.Include(x => x.Playerinfo).Select(x => new PlayerModel()
            {
                AccountCreated = x.CreatedAt,
                AccountId = x.Id,
                AccountName = x.Username,
                AchievementPoints = x.Playerinfo.Ap.GetValueOrDefault(0),
                Karma = x.Playerinfo.Karma.GetValueOrDefault(0),
                SF = x.Playerinfo.Sf.GetValueOrDefault(0),
                Clan = clans.Where(y => y.Id == x.Id).Select(y => y.ClanName).FirstOrDefault(),
                LastLoggedIn = x.UpdatedAt,
                Level = x.Playerinfo.Level.GetValueOrDefault(0)
            }).ToListAsync());
        }

        [HttpGet("search")]
        [ProducesResponseType(typeof(List<string>), 200)]
        public async Task<IActionResult> SearchUsers([FromQuery] string query)
        {
            var matchingUsernames = await _context.Users
                .Where(x => x.Username.Contains(query))
                .Select(x => x.Username)
                .ToListAsync();

            return Ok(matchingUsernames);
        }
    }
}