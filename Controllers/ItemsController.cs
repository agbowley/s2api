using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.MasterServer;
using S2Api.Services;

namespace S2Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly MasterServerContext _context;
        private readonly IEventService _eventService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ItemsController(MasterServerContext context, IEventService eventService, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _eventService = eventService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("active")]
        public async Task<IActionResult> GetActiveItems()
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var currentDate = DateOnly.FromDateTime(DateTime.UtcNow);

                var activeItems = await _context.Items
                    .Where(item => item.AccountId == int.Parse(accountId) && (item.ExpDate == null || item.ExpDate > currentDate))
                    .Select(item => new Item
                    {
                        Id = item.Id,
                        AccountId = item.AccountId,
                        Type = item.Type.Value,
                        ExpDate = item.ExpDate.Value.ToDateTime(TimeOnly.MinValue)
                    }).ToListAsync();

                return Ok(activeItems);
            }
            catch (Exception ex)
            {
                // Handle the exception or return an error response
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving items.");
            }
        }

        [HttpGet("stored")]
        public async Task<IActionResult> GetStoredItems()
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Where(s => s.AccountId == int.Parse(accountId))?
                    .FirstOrDefaultAsync();

                if (userStash == null)
                    return BadRequest("Invalid request");

                var storedItems = await _context.StoredItems
                    .Where(s => s.StashId == userStash.Id && s.ModStage == 4)
                    .Select(item => new StoredItem
                    {
                        Id = item.Id,
                        Type = item.Type.Value,
                        CreationDate = item.CreationDate.Value,
                    }).ToListAsync();

                return Ok(storedItems);
            }
            catch (Exception ex)
            {
                // Handle the exception or return an error response
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving items.");
            }
        }

        [HttpPost("generate")]
        public async Task<IActionResult> GenerateNewItem()
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = _context.UserStashes
                    .Where(s => s.AccountId == int.Parse(accountId))?
                    .FirstOrDefault();

                if (userStash == null)
                    return BadRequest("Invalid request");

                var itemToModify = await _context.StoredItems
                    .Where(s => s.StashId == userStash.Id)?
                    .FirstOrDefaultAsync(item => item.ModStage < 4);

                if (itemToModify != null && itemToModify.Type.HasValue && itemToModify.ModStage.HasValue)
                    return Ok(new { type = ItemHelpers.GetCurrentAffixValue(itemToModify.Type.Value, itemToModify.ModStage.Value), stage = itemToModify.ModStage.Value });

                int newItemNumber = ItemHelpers.GenerateRandomItemNumber();

                var newItem = new Models.MasterServer.StoredItem
                {
                    StashId = userStash.Id,
                    Type = newItemNumber,
                    ModStage = 0,
                    CreationDate = DateTime.UtcNow
                };

                userStash.Gold -= 50;

                await _context.StoredItems.AddAsync(newItem);
                await _context.SaveChangesAsync();

                var eventData = new { gold = userStash.Gold.ToString() };
                await _eventService.SendEventAsync(int.Parse(accountId), "user-stash-updated-event", eventData);

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while generating a new item.");
            }
        }

        [HttpPost("modify")]
        [HttpPost("modify/{affix}")]
        public async Task<IActionResult> ModifyItem([FromRoute] int? affix = null)
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                var itemToModify = userStash.StoredItems
                    .FirstOrDefault(item => item.ModStage < 4);

                if (itemToModify == null)
                    return BadRequest("Invalid request");

                /* Perform a purchase */
                if (affix.HasValue)
                {
                    if (itemToModify.ModStage.Value > 3)
                        return BadRequest("Invalid request");

                    var digits = itemToModify.Type.ToString().ToCharArray();
                    digits[itemToModify.ModStage.Value] = affix.Value.ToString()[0];
                    var newItemType = int.Parse(new string(digits));

                    if (ItemHelpers.IsValidItem(newItemType) == false)
                        return BadRequest("Invalid affix type chosen.");

                    var itemPrice = AffixCosts.GetCost(affix.Value, itemToModify.ModStage.Value);

                    if (userStash.Gold < itemPrice)
                        return BadRequest("Not enough gold.");

                    itemToModify.Type = newItemType;
                    userStash.Gold -= itemPrice;
                }

                /* Increment the modification stage */
                itemToModify.ModStage++;
                await _context.SaveChangesAsync();

                var eventData = new { gold = userStash.Gold.ToString() };
                await _eventService.SendEventAsync(int.Parse(accountId), "user-stash-updated-event", eventData);

                return Ok(new { type = ItemHelpers.GetCurrentAffixValue(itemToModify.Type.Value, itemToModify.ModStage.Value), stage = itemToModify.ModStage.Value });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while modifying the item.");
            }
        }

        [HttpPost("salvage/{itemId}")]
        public async Task<IActionResult> SalvageItem([FromRoute] int itemId)
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                var itemToSalvage = userStash.StoredItems.FirstOrDefault(item => item.Id == itemId);

                if (itemToSalvage == null)
                    return BadRequest("Invalid request");

                if (itemToSalvage.ModStage < 4)
                    return BadRequest("Cannot salvage an incomplete item.");

                // Calculate the salvage value (50% of the item cost)
                int salvageValue = 50; // Initial cost
                string itemType = itemToSalvage.Type.ToString();

                for (int i = 0; i < itemType.Length; i++)
                {
                    var affix = itemType[i].ToString();
                    var affixType = int.Parse(affix);
                    salvageValue += AffixCosts.GetCost(affixType, i);
                }

                salvageValue /= 2;

                // Update user stash gold
                userStash.Gold += salvageValue;

                // Remove the item from user stash
                userStash.StoredItems.Remove(itemToSalvage);

                // Save changes to the database
                await _context.SaveChangesAsync();

                var eventData = new { gold = userStash.Gold.ToString() };
                await _eventService.SendEventAsync(int.Parse(accountId), "user-stash-updated-event", eventData);

                return Ok(new { message = $"Item salvaged. Gold added: {salvageValue}" });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while salvaging the item.");
            }
        }

        [HttpPost("activate/{itemId}")]
        public async Task<IActionResult> ActivateItem([FromRoute] int itemId)
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                var itemToMove = userStash.StoredItems.FirstOrDefault(item => item.Id == itemId);

                if (itemToMove == null)
                    return BadRequest("Invalid request");

                if (itemToMove.ModStage < 4)
                    return BadRequest("Cannot move an incomplete item.");

                // Additional validation: Check if user already has the maximum number of active items
                var activeItemsCount = await _context.Items
                    .Where(i => i.AccountId == userStash.AccountId && i.ExpDate > DateOnly.FromDateTime(DateTime.UtcNow))
                    .CountAsync();

                if (activeItemsCount >= 5)
                    return BadRequest("You already have the maximum number of active items.");

                // Add the item to the Items table
                _context.Items.Add(new Models.MasterServer.Item()
                {
                    AccountId = userStash.AccountId,
                    ExpDate = DateOnly.FromDateTime(DateTime.UtcNow.AddMonths(3)),
                    Type = itemToMove.Type
                });

                // Remove the item from storedItems
                userStash.StoredItems.Remove(itemToMove);

                // Save changes to the database
                await _context.SaveChangesAsync();

                var eventData = new { action = "moved", itemId = itemToMove.Id, gold = userStash.Gold.ToString() };
                await _eventService.SendEventAsync(int.Parse(accountId), "user-stash-updated-event", eventData);

                return Ok(new { message = "Item moved to Items." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while moving the item.");
            }
        }

        [HttpPost("delete-active/{itemId}")]
        public async Task<IActionResult> DeleteActiveItem([FromRoute] int itemId)
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                var itemToDelete = await _context.Items.FirstOrDefaultAsync(item => item.Id == itemId);

                if (itemToDelete == null || itemToDelete.AccountId != int.Parse(accountId))
                    return BadRequest("Invalid request");

                if (itemToDelete.ExpDate <= DateOnly.FromDateTime(DateTime.UtcNow))
                    return BadRequest("Cannot delete an expired item.");

                // Uncomment the following lines for soft delete (set ExpDate to tomorrow and save)
                // itemToDelete.ExpDate = DateOnly.FromDateTime(DateTime.UtcNow.AddDays(1));
                // _context.Items.Update(itemToDelete);

                // Delete the item from the Items table
                _context.Items.Remove(itemToDelete);

                // Save changes to the database
                await _context.SaveChangesAsync();

                var eventData = new { action = "delete", itemId = itemToDelete.Id, gold = userStash.Gold.ToString() };
                await _eventService.SendEventAsync(int.Parse(accountId), "user-stash-updated-event", eventData);

                return Ok(new { message = "Active item deleted." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while deleting the item.");
            }
        }



        [HttpGet()]
        public async Task<IActionResult> GetItem()
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                var itemToModify = userStash.StoredItems
                    .FirstOrDefault(item => item.ModStage < 4);

                if (itemToModify == null)
                    return NotFound();

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while modifying the item.");
            }
        }

        [HttpGet("gold")]
        public async Task<IActionResult> GetGold()
        {
            try
            {
                var accountId = User.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

                if (string.IsNullOrWhiteSpace(accountId))
                    return Forbid();

                var userStash = await _context.UserStashes
                    .Include(s => s.StoredItems)
                    .FirstOrDefaultAsync(s => s.AccountId == int.Parse(accountId));

                if (userStash == null || userStash.StoredItems == null)
                    return BadRequest("Invalid request");

                return Ok(userStash.Gold);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while modifying the item.");
            }
        }

        public class Item
        {
            public int Id { get; set; }
            public int AccountId { get; set; }
            public int Type { get; set; }
            public int ModStage { get; set; }
            public DateTime ExpDate { get; set; }
        }

        public static class AffixCosts
        {
            public static int GetCost(int affixId, int stageId)
            {
                switch (stageId)
                {
                    case 0: // Type
                        return GetTypeCost(affixId);
                    case 1: // Regen
                        return GetRegenCost(affixId);
                    case 2: // Passive
                        return GetPassiveCost(affixId);
                    case 3: // Active
                        return GetActiveCost(affixId);
                    default:
                        throw new ArgumentException("Invalid stage id.");
                }
            }

            private static int GetTypeCost(int affixId)
            {
                switch (affixId)
                {
                    case 1: // Ring
                        return 5;
                    case 2: // Amulet
                        return 10;
                    case 3: // Jewel
                        return 15;
                    case 4: // Rune
                        return 25;
                    default:
                        throw new ArgumentException("Invalid type affix id.");
                }
            }

            private static int GetRegenCost(int affixId)
            {
                switch (affixId)
                {
                    case 1: // Red
                        return 5;
                    case 3: // Blue
                        return 10;
                    case 4: // White
                        return 10;
                    default:
                        throw new ArgumentException("Invalid regen affix id.");
                }
            }

            private static int GetPassiveCost(int affixId)
            {
                switch (affixId)
                {
                    case 1: // Dolphin
                        return 15;
                    case 2: // Beaver
                        return 10;
                    case 4: // Armadillo
                        return 20;
                    case 5: // Bear
                        return 20;
                    case 6: // Rabbit
                        return 10;
                    default:
                        throw new ArgumentException("Invalid passive affix id.");
                }
            }

            private static int GetActiveCost(int affixId)
            {
                switch (affixId)
                {
                    case 1: // Lungs
                    case 2: // Heart
                    case 3: // Shield
                    case 4: // Feet
                    case 5: // Brain
                    case 6: // Dagger
                        return 15;
                    default:
                        throw new ArgumentException("Invalid active affix id.");
                }
            }
        }

        public static class ItemHelpers
        {
            public static int GetDigit(int n, int i)
            {
                if (n < 0 || i < 1)
                    throw new ArgumentException("n must be a non-negative integer and i must be a positive integer");

                string nStr = n.ToString();

                if (i > nStr.Length)
                    throw new ArgumentException("i > length");

                char cDigit = nStr[i];
                int digit = int.Parse(cDigit.ToString());

                return digit;
            }

            public static int GetCurrentAffixValue(int type, int stage)
            {
                if (stage == 0)
                    return 0;

                return int.Parse(type.ToString()[..stage].ToString());
            }

            public static int GenerateRandomItemNumber()
            {
                var random = new Random();

                int[] allowedDigits1 = { 1, 2, 3, 4 };
                int[] allowedDigits2 = { 1, 3, 4 };
                int[] allowedDigits3 = { 1, 2, 4, 5, 6 };
                int[] allowedDigits4 = { 1, 2, 3, 4, 5, 6 };

                int digit1 = allowedDigits1[random.Next(allowedDigits1.Length)];
                int digit2 = allowedDigits2[random.Next(allowedDigits2.Length)];
                int digit3 = allowedDigits3[random.Next(allowedDigits3.Length)];
                int digit4 = allowedDigits4[random.Next(allowedDigits4.Length)];

                return digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;
            }

            public static bool IsValidItem(int newItemNumber)
            {
                int[] allowedDigits1 = { 1, 2, 3, 4 };
                int[] allowedDigits2 = { 1, 3, 4 };
                int[] allowedDigits3 = { 1, 2, 4, 5, 6 };
                int[] allowedDigits4 = { 1, 2, 3, 4, 5, 6 };

                int digit1 = newItemNumber / 1000;
                int digit2 = (newItemNumber / 100) % 10;
                int digit3 = (newItemNumber / 10) % 10;
                int digit4 = newItemNumber % 10;

                bool isValid =
                    allowedDigits1.Contains(digit1) &&
                    allowedDigits2.Contains(digit2) &&
                    allowedDigits3.Contains(digit3) &&
                    allowedDigits4.Contains(digit4);

                return isValid;
            }
        }
    }
}